#!/usr/bin/python
#
# 3g_signal.py
#
# version 1.0 July 2017
#
# copyright (c) Shore2ship BV
#
import serial, time,  mmap,os
ser=serial.Serial('/dev/ttyUSB2',9600,timeout=1)
ser.write('AT+CSQ\x0D')   	#get signal strength
time.sleep(1)
ser.readline()			#dummy readline
out=ser.readline()
parts=out.partition(':')
signal=parts[2].partition(',')
print signal[0]
