###############################################################################
#
# autoswitch.sh
#
# version 0.2 March 2017
#
# copyright (c) shore2ship
#
# pingtests primary route, if pingtest fails, alternate default gw is added
# primary default gw is removed
#
# 0.2: tests for existing default route
# 0.4: removes default route which is not used
# 0.5: adds pingroute if it doesnt exist
#
# usage: run this script as often as you like (every minute?)
#	 - create persistent route for pingadress over primaryroute
#    - make a firewall rule for "ping" which is disabled when limit is met
#	   or country is not in allowed list
###############################################################################
pingaddress=4.2.2.2
primaryroute=192.168.2.231
secondaryroute=192.168.2.10
#

prim_routeexists=$(route|grep default|grep $primaryroute >/dev/null && echo yes || echo no)
sec_routeexists=$(route|grep default|grep $secondaryroute >/dev/null && echo yes || echo no)
ping_routeexists=$(route|grep $pingaddress|grep $primaryroute >/dev/null && echo yes || echo no)
echo Primary route: $prim_routeexists
echo Secondary route: $sec_routeexists
if [ $ping_routeexists == no];
then
	route add $pingaddress netmask 0.0.0.0 gw $primaryroute
fi
connected=$(ping -q -w 10 -c 1 $pingaddress >/dev/null && echo ok || echo error)
if [ $connected == ok ]; 
then
	echo "Primary route online"	
	if [ $prim_routeexists == no ];
	then
		echo Setting up primary route
		route add default gw $primaryroute
		route delete default gw $secondaryroute
	fi
	if [ $sec_routeexists == yes ];
	then
		echo removing secondary route...
		route delete default gw $secondaryroute
	fi
else
	echo "Secondary route enabled"
	if [ $sec_routeexists == no ];
	then
		echo Setting up secondary route
		route add default gw $secondaryroute
		route delete default gw $primaryroute
	fi
	if [ $prim_routeexists == yes ];
	then
		echo removing primary route...
		route delete default gw $primaryroute
	fi
fi
