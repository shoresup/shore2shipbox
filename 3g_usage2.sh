#!/bin/sh
limit=$(uci get cbi_file.x.datalimit)
echo "limit=$limit KB"
rx=$(vnstat --oneline|awk -F";" '{print $9}')
rxunits=$(echo $rx|awk '{print $2}')
rxvalueint=$(echo $rx|awk '{print $1}'| awk -F"." '{print $1}')
rxvaluedec=$(echo $rx|awk '{print $1}'| awk -F"." '{print $2}')
if [ $rxunits = "MiB" ] && [ -n "$rxvaluedec" ]
then
	rxv=$(( $rxvalueint \* 1024 + ($rxvaluedec \* 1024)/100 ))
else
	rxv=$(( $rxvalueint ))
fi
tx=$(vnstat --oneline|awk -F";" '{print $10}')
echo "tx=$tx"
txvalueint=$(echo $tx|awk '{print $1}'| awk -F"." '{print $1}')
txvaluedec=$(echo $tx|awk '{print $1}'| awk -F"." '{print $2}')
txunits=$(echo $tx|awk '{print $2}')
if [ $rxunits = "MiB" ] && [ -n "$txvaluedec" ]
then
	echo "hallo"
	txv=$(( $txvalueint \* 1024 + ($txvaluedec \* 1024)/100 ))
else
	txv=$(( $txvalueint ))
fi
totaluseage=$(( $txv + $rxv ))
echo "{ rx: $rxv , tx: $txv , tot: $totaluseage }"

if [ $totaluseage -gt $limit ]
then
	echo "Limit exceeded applying firewall rule"
	./firewallrule.sh windmeter disable
fi
