#!/bin/sh

baseadress="192.168.1."
i=0
while [ $i -lt 255 ];
do  
        connected=$(ping -q -w 10 -c 1 $baseadress$i >/dev/null && echo ok || echo error)
        echo $baseadress$i $connected
        i=$(( $i + 1 )) 
done
