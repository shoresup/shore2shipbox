#**********************
#
# 3g_checkcountry.sh
#
# copyright (c) 2016 Shore2ship BV
#
# v 1.1 June 2016
#
# ash shell script to check for allowed country
#
# read settings:
source /root/scripts/config.sh
#
#
opcheck=$(/root/scripts/3g_opcheck.py)
echo "|$opcheck|"
notfound='country=nf'
if [ "$opcheck" == "$notfound" ]
then
	echo "country unknown" 
	>&2 echo "Country unknown --> applying firewall rules" 
	uci show shore2ship.3g.rules|awk -F"=" '{print $2}' |tr ' ' '\n'|
	while read rule
	do
		>&2 echo "rule=$rule"
		/root/scripts/firewallrule.sh $rule disable
	done
fi
if [ "$opcheck" = "not registered" ]
then	
	echo "not registered" >>$LOGFILE
	exit
fi
