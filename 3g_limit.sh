#!/bin/sh
id=$(uci get shore2ship.general.serial)
limit=$(uci get shore2ship.3g.datalimit)
date=$(date '+%m/%d/%Y %H:%M')
echo "{ timestamp: \"$date\" , id: $id , metric:86, ttl:3600, value: $limit}"
