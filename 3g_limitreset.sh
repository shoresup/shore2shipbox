#!/bin/sh
limit=$(uci get shore2ship.3g.datalimit)
totalusage=$(uci get shore2ship.status.usage)
echo "{ rx: $rxv , tx: $txv , tot: $totalusage , limit: $limit}"
opcheck=$(/root/scripts/3g_opcheck.py)
echo $opcheck
if [ $totalusage -lt $limit ] && [ "$opcheck" != "country=nf" ]
then
	echo "Below Limit and allowed country --> resetting firewall rules"
	uci show shore2ship.3g.rules|awk -F"=" '{print $2}' |tr ' ' '\n'|
	while read rule
	do
		echo "rule=$rule"
		/root/scripts/firewallrule.sh $rule enable
	done
fi
