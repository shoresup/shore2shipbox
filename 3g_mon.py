#!/usr/bin/python
import serial, time,  mmap
print 'test'
ser=serial.Serial('/dev/ttyUSB2',9600,timeout=1)
ser.write('ATE0')
x=0;
f=open('/root/temp/3glog','a')

def find_operator(opcode):
	opcode=opcode[1:6]
	alt_opcode = opcode[0:3] + '\x09' + opcode[3:6]
	with open('passport_operators.txt','r') as inf:
		for line in inf:
			if opcode in line:
				return line[7:]
			elif alt_opcode in line:
				return line[7:]
	return '--nf--'




while True:
	f.write(time.strftime("%a, %d %b %Y %H:%M:%S",time.gmtime())+ '\n')
	ser.write('AT+COPS?\x0D')
	time.sleep(1)
	out6= ser.readline()
	out1= ser.readline()
	out2= ser.readline()
	parts = out2.partition(':')
	if parts[0] == '+COPS':
		values = parts[2].split(',')
		print 'Operator=' + values[2]	
		print 'Op name=' + find_operator(values[2])
		f.write( 'Operator=' + values[2] + '\n')
	elif parts[0] == '+CSQ':
		values = parts[2].split(',')
		csq = int(values[0])
		print 'Signal Strength=' + str(csq)
		f.write( 'Signal Strenth=' + str(csq) + '\n')
		if csq > 14:
			print 'Strong Signal'
		elif csq > 9:
			print 'Moderate Signal'
		else:
			print 'Weak Signal'
	if len(out2) > 4:
		print 'out:' + out2 
	ser.write('AT+CSQ\x0D')
	x = x + 1
	out4=ser.readline()
	out3=ser.readline()
	f.flush()
	out5=ser.readline()
