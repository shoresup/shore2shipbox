# scripts to check and repaire file system
#
panic=$(dmesg|grep "deleted inode"|wc -l)
if [ $panic -gt 0 ]
then
	echo "attempting fs repair.."
	e2fsck -p /dev/sda2
	reboot
fi
