276	02	Vodafone	Albania
213	03	Mobiland	Andorra
505	02	Optus	Australia
505	01	Telstra	Australia
505	03	Vodafone	Australia
232	11	Mobilkom	Austria
232	01	Mobilkom	Austria
232	05	One	Austria
232	03	T-Mobile	Austria
232	07	Tele.Ring	Austria
206	20	Base	Belgium
206	10	Mobistar	Belgium
206	01	Proximus	Belgium
284	05	Globul	Bulgaria
284	01	M-Tel	Bulgaria
280	10	Areeba LTD	Cyprus
280	01	Cytamobile-Vodafone	Cyprus
230	02	Eurotel	Czech Republic
230	03	Oskar-Vodafone	Czech Republic
230	01	T-Mobile	Czech Republic
238	02	Sonofon	Denmark
238	01	Tele Danmark Mobil	Denmark
602	02	Vodafone	Egypt
248	02	Elisa	Estonia
248	01	EMT	Estonia
244	05	Elisa	Finland
244	91	Telia Sonera	Finland
208	20	Bouygues	France
208	01	Orange	France
208	10	SFR	France
262	01	T-Mobile	Germany
262	02	Vodafone	Germany
202	05	Vodafone	Greece
202	10	WIND	Greece
234	03	Airtel-Vodafone	Guernsey
234	55	Cable & Wireless	Guernsey
234	50	Jersey Telecom	Guernsey
216	30	T Mobile	Hungary
216	70	Vodafone	Hungary
214	03	Orange	Ibiza (Spain)
214	01	Vodafone	Ibiza (Spain)
214	06	Vodafone Espa�a, S.A.U.	Ibiza (Spain)
274	01	S�minn hf	Iceland
274	02	Vodafone	Iceland
272	02	O2	Ireland
272	01	Vodafone	Ireland
222	01	Telecom Italia Mobile	Italy
222	10	Vodafone	Italy
222	88	Wind	Italy
440	10	NTT DoCoMo, Inc	Japan
440	20	Softbank	Japan
234	03	Airtel-Vodafone	Jersey
234	55	Cable & Wireless	Jersey
234	50	Jersey Telecom	Jersey
202	05	Vodafone	Kreta (Greece)
202	10	WIND	Kreta (Greece)
247	05	Bite Latvija	Latvia
247	01	LMT	Latvia
228	01	Swisscom	Liechtenstein
246	02	Bite GSM	Lithuania
246	01	Omnitel	Lithuania
270	01	LUXGSM-Vodafone	Luxembourg
270	77	Tango	Luxembourg
270	99	VOXmobile	Luxembourg
268	03	Sonaecom (Optimus)	Madeira (Portugal)
268	06	TMN	Madeira (Portugal)
268	01	Vodafone	Madeira (Portugal)
214	03	Orange	Mallorca (Spain)
214	01	Vodafone	Mallorca (Spain)
214	06	Vodafone Espa�a, S.A.U.	Mallorca (Spain)
278	21	go mobile	Malta
901	28	VF Malta	Malta
278	01	Vodafone	Malta
214	01	Vodafone	Melilla (Spain)
214	06	Vodafone Espa�a, S.A.U.	Melilla (Spain)
214	03	Orange	Menorca (Spain)
214	01	Vodafone	Menorca (Spain)
214	06	Vodafone Espa�a, S.A.U.	Menorca (Spain)
208	20	Bouygues	Monaco
208	01	Orange	Monaco
208	10	SFR	Monaco
204	04	Vodafone	Netherlands
286	01	Turkcell	Northern Cyprus
286	02	Vodafone Turkey	Northern Cyprus
234	33	Everything Everywhere Ltd(Orange)	Northern Ireland (United Kingdom)
234	30	Everything Everywhere Ltd(T-Mobile)	Northern Ireland (United Kingdom)
234	10	O2	Northern Ireland (United Kingdom)
234	15	Vodafone	Northern Ireland (United Kingdom)
242	02	Netcom	Norway
242	01	Telenor	Norway
260	02	Era	Poland
260	03	Orange	Poland
260	01	Plus	Poland
268	03	Sonaecom (Optimus)	Portugal
268	06	TMN	Portugal
268	01	Vodafone	Portugal
310	38	AT&T	Puerto Rico
310	41	AT&T	Puerto Rico
310	26	T-Mobile	Puerto Rico
427	01	Q-Tel	Qatar
202	05	Vodafone	Rhodos (Greece)
202	10	WIND	Rhodos (Greece)
226	01	Vodafone RO	Romania
222	01	Telecom Italia Mobile	Sardinia (Italy)
222	10	Vodafone	Sardinia (Italy)
222	88	Wind	Sardinia (Italy)
234	33	Everything Everywhere Ltd(Orange)	Scotland (United Kingdom)
234	30	Everything Everywhere Ltd(T-Mobile)	Scotland (United Kingdom)
234	10	O2	Scotland (United Kingdom)
234	15	Vodafone	Scotland (United Kingdom)
222	01	Telecom Italia Mobile	Sicilia (Italy)
222	10	Vodafone	Sicilia (Italy)
222	88	Wind	Sicilia (Italy)
231	01	Orange	Slovakia
231	02	T-Mobile	Slovakia
293	41	Mobitel	Slovenia
293	40	SI Mobil - Vodafone	Slovenia
214	03	Orange	Spain
214	01	Vodafone	Spain
214	06	Vodafone Espa�a, S.A.U.	Spain
240	07	Tele 2 Sverige AB	Sweden
240	08	Telenor	Sweden
240	01	Telia	Sweden
228	03	Orange	Switzerland
228	02	Sunrise	Switzerland
228	01	Swisscom	Switzerland
286	03	AVEA	Turkey
286	04	Avea Iletisim Hizmetleri A.S.	Turkey
286	01	Turkcell	Turkey
286	02	Vodafone Turkey	Turkey
234	07	Cable&Wireless	United Kingdom
234	33	Everything Everywhere Ltd(Orange)	United Kingdom
234	30	Everything Everywhere Ltd(T-Mobile)	United Kingdom
234	10	O2	United Kingdom
234	15	Vodafone	United Kingdom
