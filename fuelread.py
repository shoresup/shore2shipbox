#!/usr/bin/python

import RPi.GPIO as GPIO
import time
import spidev
import sys
import serial
#import pispi_rdu_lib as RDU


spi = spidev.SpiDev()  # spidev normally installed with RPi 3 distro's
                            # Make Sure SPI is enabled in RPi preference

GPIO.setmode(GPIO.BCM)  # Use RPi GPIO numbers
GPIO.setwarnings(False) # disable warnings

BAUD_RATE = 19200
TIME_OUT = 1
DIR_DELAY = .01

#define GPIO Pins
DIR_RS485 = 25

# Relays
K1 = 5
K2 = 6
K3 = 13
K4 = 19

# RS485 or HART Uart Switch
H_EN = 17

# Digital Input Chip Select
CS_DI = 26

# Aanalog Input Chip Selects
CS_SDAFE_1 = 21
CS_SDAFE_2 = 20
CS_SDAFE_3 = 16
CS_SDAFE_4 = 12

# PI-SPI-DIN Expansion Modules Chip Selects
CE1 = 7
CE0 = 8
CE2 = 24
CE3 = 23
CE4 = 18

# Remote Display Modbus ID
RDU_ID = 100

AN_TYPE_1_MA = 1        # Input 0 to 20 mA, Returns 0 to 20000, Scaler 1000
AN_TYPE_2_MA = 2        # Input 0 to 20 mA, Returns 0 to 10000, Scaler 100
AN_TYPE_3_MA = 3        # Input 4 to 20 mA, Returns 0 to 10000, Scaler 100 Note: Readings below 4 mA return as 0

AN_TYPE_4_5VDC = 4      # Input 0 to 5 VDC, Returns 0 to 5000, Scaler 1000
AN_TYPE_5_5VDC = 5      # Input 0 to 5 VDC, Returns 0 to 10000, Scaler 100
AN_TYPE_6_5VDC = 6      # Input 1 to 5 VDC, Returns 0 to 10000, Scaler 100 Note: Readngs beluw 1 VDC return as 0

AN_TYPE_7_10VDC = 7     # Input 0 to 10 VDC, Returns 0 to 10000, Scaler 1000
AN_TYPE_8_10VDC = 8     # Input 0 to 10 VDC, Returns 0 to 10000, Scaler 100
AN_TYPE_9_10VDC = 9     # Input 2 to 10 VDC, Returns 0 to 10000, Scaler 100 Note: Readings below 2 VDC return as 0

AN_1 = 1
AN_2 = 2
AN_3 = 3
AN_4 = 4
AN_5 = 5
AN_6 = 6
AN_7 = 7
AN_8 = 8
AN_9 = 9


# Intialize GPIO Pins
GPIO.setup(DIR_RS485,GPIO.OUT)      # RS485 DIR bit
GPIO.output(DIR_RS485,0)            # Set RS485 to Read

GPIO.setup(K1,GPIO.OUT)             # K1
GPIO.output(K1,0)
GPIO.setup(K2,GPIO.OUT)             # K2
GPIO.output(K2,0)
GPIO.setup(K3,GPIO.OUT)             # K3
GPIO.output(K3,0)
GPIO.setup(K4,GPIO.OUT)             # K4
GPIO.output(K4,0)

GPIO.setup(H_EN,GPIO.OUT)           # RS485 OR Hart UART Select
GPIO.output(H_EN,1)                 # Enable RS485

GPIO.setup(CS_DI,GPIO.OUT)          # Digital Inputs Chip Select    - Active Low
GPIO.output(CS_DI,1)

GPIO.setup(CS_SDAFE_1,GPIO.OUT)     # Analog Input Module 1 Chip Select - Active Low
GPIO.output(CS_SDAFE_1,1)
GPIO.setup(CS_SDAFE_2,GPIO.OUT)     # Analog Input Module 2 Chip Select - Active Low
GPIO.output(CS_SDAFE_2,1)
GPIO.setup(CS_SDAFE_3,GPIO.OUT)     # Analog Input Module 3 Chip Select - Active Low
GPIO.output(CS_SDAFE_3,1)
GPIO.setup(CS_SDAFE_4,GPIO.OUT)     # Analog Input Module 4 Chip Select - Active Low
GPIO.output(CS_SDAFE_4,1)

GPIO.setup(CE1,GPIO.OUT)            # PI-SPI-DIN Series CHip Select 1 - Active Low
GPIO.output(CE1,1)
GPIO.setup(CE0,GPIO.OUT)            # PI-SPI-DIN Series CHip Select 0 - Active Low
GPIO.output(CE0,1)
GPIO.setup(CE2,GPIO.OUT)            # PI-SPI-DIN Series CHip Select 2 - Active Low
GPIO.output(CE2,1)
GPIO.setup(CE3,GPIO.OUT)            # PI-SPI-DIN Series CHip Select 3 - Active Low
GPIO.output(CE3,1)
GPIO.setup(CE4,GPIO.OUT)            # PI-SPI-DIN Series CHip Select 4 - Active Low
GPIO.output(CE4,1)

# Open Serila port for RS485 Drive Chip
#rs485 = serial.Serial("/dev/ttyS0", baudrate=19200, timeout=3.0)

DI_Inputs = [0,0,0,0,0,0,0,0]

AN_Type = [0,0,0,0,0]
AN_AD_Counts = [0,0,0,0,0]
AN_Scaler = [0,0,0,0,0]
AN_Status = [0,0,0,0,0]
AN_Reading = [0,0,0,0,0]
AN_Display = ["","","","",""]


def Program_SDAFE_Type(channel, type):
    buf = [0] * 10
    buf[0] = 6                  # SDAFE Commad to program type
    buf[1] = type
    buf[2] = 0

    if channel == 1:
        GPIO.output(CS_SDAFE_1,0)
        spi.writebytes(buf)
        GPIO.output(CS_SDAFE_1,1)

    if channel == 2:
        GPIO.output(CS_SDAFE_2,0)
        spi.writebytes(buf)
        GPIO.output(CS_SDAFE_2,1)

    if channel == 3:
        GPIO.output(CS_SDAFE_3,0)
        spi.writebytes(buf)
        GPIO.output(CS_SDAFE_3,1)

    if channel == 4:
        GPIO.output(CS_SDAFE_4,0)
        spi.writebytes(buf)
        GPIO.output(CS_SDAFE_4,1)

    return

def Update_SDAFE(channel):
    buf = [0] * 10
    data = [0] * 10
    buf[0] = 3                  # SDAFE Commad to read input

    if channel == 1:
        GPIO.output(CS_SDAFE_1,0)
        data = spi.xfer(buf)
        GPIO.output(CS_SDAFE_1,1)

    if channel == 2:
        GPIO.output(CS_SDAFE_2,0)
        data = spi.xfer(buf)
        GPIO.output(CS_SDAFE_2,1)

    if channel == 3:
        GPIO.output(CS_SDAFE_3,0)
        data = spi.xfer(buf)
        GPIO.output(CS_SDAFE_3,1)

    if channel == 4:
        GPIO.output(CS_SDAFE_4,0)
        data = spi.xfer(buf)
        GPIO.output(CS_SDAFE_4,1)

    #print(data)
    if data[0] != 0x55:
        AN_Display[channel] = "A%d SPI Error" % channel
        return

    AN_Type[channel] = data[1]
    AN_AD_Counts[channel] = (data[6] * 256) + data[7]
    AN_Scaler[channel] = (data[4] * 256) + data[5]

    if AN_Scaler[channel] == 0:
        AN_Display[channel] = "A%d Divide by Zero Error" % channel
        return

    AN_Status[channel] = data[8]

    if AN_Status[channel] == 0x01:
        AN_Display[channel] = "A%d Polarity Error" % channel
        return

    if AN_Status[channel] == 0x02:
        AN_Display[channel] = "A%d Overange Error" % channel
        return

    AN_Reading[channel] = ((data[2] * 256) + data[3])*10
    AN_Display[channel] = "A%d= %7.3f" % (channel, (float)(AN_Reading[channel])/AN_Scaler[channel])

    if AN_Type[channel] == AN_TYPE_9_10VDC:
        AN_Display[channel] += " % 2-10VDC"

    if AN_Type[channel] == AN_TYPE_8_10VDC:
        AN_Display[channel] += " % 0-10VDC"

    if AN_Type[channel] == AN_TYPE_7_10VDC:
        AN_Display[channel] += " VDC (10)"

    if AN_Type[channel] == AN_TYPE_6_5VDC:
        AN_Display[channel] += " % 1-5VDC"

    if AN_Type[channel] == AN_TYPE_5_5VDC:
        AN_Display[channel] += " % 0-5VDC"

    if AN_Type[channel] == AN_TYPE_4_5VDC:
        AN_Display[channel] += " VDC (5)"

    if AN_Type[channel] == AN_TYPE_3_MA:
        AN_Display[channel] += " liter/hr"

    if AN_Type[channel] == AN_TYPE_2_MA:
        AN_Display[channel] += " % 0-20mA"

    if AN_Type[channel] == AN_TYPE_1_MA:
        AN_Display[channel] += " mA"

#    print (AN_Display[channel])

    return ((float)(AN_Reading[channel]/AN_Scaler[channel]))


def Initialize_Digital_Inputs():
    buf = [0] * 7
    addr = 0x40
    buf[0] = addr               # Write Command
    buf[1] = 0x00               # Set Address IODIR
    buf[2] = 0xff               # Set IODIR to Inputs
    buf[3] = 0xff               # Set IPOL Polarity to Invert
    buf[4] = 0x00               # Set GPINTEN
    buf[5] = 0x00               # Set DEFVAL
    buf[6] = 0x00               # Set INTCON

    GPIO.output(CS_DI,0)
    spi.writebytes(buf)
    GPIO.output(CS_DI,1)
    return

def Update_Digital_Inputs():
    buf = [0] * 2
    addr = 0x41
    buf[0] = addr               # Read Command
    buf[1] = 0x09               # Register Address

    GPIO.output(CS_DI,0)
    spi.writebytes(buf)
    data = spi.readbytes(1)
    GPIO.output(CS_DI,1)
    #print ('DATA:'+str(data[0]))

    if (data[0] & 0x01) != 0:
        DI_Inputs[0] = 1
        GPIO.output(K1,1)
    else:
        DI_Inputs[0] = 0
        GPIO.output(K1,0)

    if (data[0] & 0x02) != 0:
        DI_Inputs[1] = 1
        GPIO.output(K2,1)
    else:
        DI_Inputs[1] = 0
        GPIO.output(K2,0)

    if (data[0] & 0x04) != 0:
        DI_Inputs[2] = 1
        GPIO.output(K3,1)
    else:
        DI_Inputs[2] = 0
        GPIO.output(K3,0)

    if (data[0] & 0x08) != 0:
        DI_Inputs[3] = 1
        GPIO.output(K4,1)
    else:
        DI_Inputs[3] = 0
        GPIO.output(K4,0)

    if (data[0] & 0x10) != 0:
        DI_Inputs[4] = 1
    else:
        DI_Inputs[4] = 0

    if (data[0] & 0x20) != 0:
        DI_Inputs[5] = 1
    else:
        DI_Inputs[5] = 0

    if (data[0] & 0x40) != 0:
        DI_Inputs[6] = 1
    else:
        DI_Inputs[6] = 0

    if (data[0] & 0x80) != 0:
        DI_Inputs[7] = 1
    else:
        DI_Inputs[7] = 0

    return data[0]





if __name__ == '__main__':
    try:
        spi.open(0,1)                       # Open SPI Channel 1 Chip Select is GPIO-7 (CE_1)
        spi.max_speed_hz = 500000

        Initialize_Digital_Inputs()
        time.sleep(1)
        Program_SDAFE_Type(1,3)
        time.sleep(.1)
        Program_SDAFE_Type(2, AN_TYPE_3_MA)
        time.sleep(.05)
        Program_SDAFE_Type(3, 9)
        time.sleep(.05)
        Program_SDAFE_Type(4, AN_1)
        time.sleep(.05)

        DI = Update_Digital_Inputs()

        totalvalue=0
        no_of_samples=60
        for x in range(no_of_samples):
            reading = Update_SDAFE(2)               # Update Analog channel 1
            totalvalue = totalvalue + reading                   # Update Analog channel 1
            time.sleep(1)

        litersperhour=totalvalue/no_of_samples
        if DI==0:
            kgsperhour=litersperhour*0.825
            fueltype='MGO'
        else:
            kgsperhour=litersperhour*0.89
            fueltype='HFO'
        print(kgsperhour,fueltype);

    except KeyboardInterrupt:   # Press CTRL C to exit Program
        spi.close()
        sys.exit(0)
