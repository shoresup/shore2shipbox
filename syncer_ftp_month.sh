#
# syncer_ftp.sh
# shell script for retrieval of logfiles from Eefting Energy fuel flow meter
# v0.2 jan  2013
# (c) 2013 Efficienship BV
# by W.Amels
#
#
source /root/scripts/config.sh
#
datum=$(date)
echo $SERIAL:$datum:starting ftp retrieval...>>$LOGFILE
month=$(date '+%Y%m')
count=0
while [ $count -le 31 ]
do
	if [ $count -le 9 ]
	then
		day="$(date '+%Y%m')0$count" 
	else
		day="$(date '+%Y%m')$count"
	fi
	echo today=$day
	curl -u master:ftp01 ftp://$FUELMON/REPORT/SAMPLE/$((month))/$((day))/|
	while read line
	do
		filename=$(echo "$line"| awk '{print $NF}')
		length=${#filename}
		echo length:$length
		if [ $length -gt 3 ]
		then 
			echo $SERIAL:$datum:ftp retrieving $filename... >>$LOGFILE
			fileout=/root/temp/F0015_$((day))_$filename
			curl -u master:ftp01 ftp://192.168.200.81/REPORT/SAMPLE/$((month))/$((day))/$filename > $fileout
			curl -u admin:PHPW -T $fileout ftp://192.168.200.1/Download/Efficienship/
			gzip -f -9 $fileout 
		fi
	done	
	count=$(($count+1))
done
echo $SERIAL:$datum:ftp retrieval completed... >>$LOGFILE
