# script to check file system
# only succeeds if file system is mounted read only
#
e2fsck -y /dev/sda1
e2fsck -y /dev/sda2
reboot
