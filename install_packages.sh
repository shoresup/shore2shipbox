opkg update
opkg install kmod-usb2
opkg install kmod-usb-ohci
opkg install kmod-usb-uhci
opkg install kmod-usb-serial
opkg install kmod-usb-serial-option
opkg install kmod-usb-serial-wwam
opkg install luci-proto-3g
opkg install curl
opkg install python
opkg install pyserial
opkg install e2fsprogs
opkg install vnstat
opkg install coreutils-sha1sum
opkg install kmod-mac80211
opkg install kmod-ath9k
opkg install samba36-server
opkg install luci-app-samba
mkdir /root/scripts
mkdir /root/temp
