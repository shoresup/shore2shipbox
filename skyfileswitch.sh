###############################################################################
#
# skyfileswitch.sh
#
# version 0.1 Ocotber 2019
#
# copyright (c) shore2ship
#
# pingtests primary route, if pingtest fails, alternate default gw is added
# primary default gw is removed
#
#
# usage: run this script as often as you like (every minute?)
#	 - create persistent route for pingadress over primaryroute
#    - make a firewall rule for "ping" which is disabled when limit is met
#	   or country is not in allowed list
###############################################################################
pingaddress=4.2.2.2
primaryroute=10.64.64.64
secondaryroute=192.168.0.2
skyfileadress=77.70.254.246
#

prim_routeexists=$(route|grep $skyfileadress|grep $primaryroute >/dev/null && echo yes || echo no)
sec_routeexists=$(route|grep $skyfileadress|grep $secondaryroute >/dev/null && echo yes || echo no)
ping_routeexists=$(route|grep $pingaddress|grep $primaryroute >/dev/null && echo yes || echo no)
echo Primary route: $prim_routeexists
echo Secondary route: $sec_routeexists
if [ $ping_routeexists == no ];
then
	route add $pingaddress netmask 0.0.0.0 gw $primaryroute
fi
connected=$(ping -q -w 10 -c 1 $pingaddress >/dev/null && echo ok || echo error)
if [ $connected == ok ]; 
then
	echo "Primary route is online"	
	if [ $prim_routeexists == no ];
	then
		echo Setting up primary route
		route add $skyfileadress netmask 0.0.0.0 gw $primaryroute 
		route delete $skyfileadress netmask 0.0.0.0 gw $secondaryroute
	fi
	if [ $sec_routeexists == yes ];
	then
		echo removing secondary route...
		route delete $skyfileadress netmask 0.0.0.0 gw $secondaryroute
	fi
else
	echo "Primary route is offline, attempting to route over secondary route "
	if [ $sec_routeexists == no ];
	then
		echo Setting up secondary route
		route add $skyfileadress netmask 0.0.0.0 gw $secondaryroute 
	fi
	if [ $prim_routeexists == yes ];
	then
		echo removing primary route...
		route delete $skyfileadress1 netmask 0.0.0.0 gw $primaryroute
	fi
fi
