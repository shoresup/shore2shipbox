#!/bin/sh
linkisup=$(ifconfig 3g-3g|grep bytes >/dev/null && echo yes || echo no)
echo $linkisup
if [ $linkisup == yes ]
then
	txv=$(ifconfig 3g-3g|grep bytes|awk -F":" '{print $2}'|awk -F" " '{print $1}')
	rxv=$(ifconfig 3g-3g|grep bytes|awk -F":" '{print $3}'|awk -F" " '{print $1}')
	usage=$(uci get shore2ship.status.3gusage)
	curusage=$(( $txv + $rxv ))
	echo current: $curusage
	lastusage=$(uci get shore2ship.status.lastusage)
	echo last: $lastusage
	if [ $curusage -ge $lastusage ]
	then
		delta=$(( $curusage - $lastusage ))
		echo delta=$delta
	else	
		echo interface reset?
		delta=$curusage
		echo delta=$delta
	fi
	echo $txv,$rxv,$usage,$curusage,$lastusage,$delta
	usage=$(( $usage + $delta ))
	uci set shore2ship.status.lastusage=$curusage
	uci set shore2ship.status.3gusage=$usage
	uci commit
fi

