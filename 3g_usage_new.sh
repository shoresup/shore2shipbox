#!/bin/sh
##############################################################################
#
# 3g_usage.sh
#
# version 1.0 June 2016
# version 2.0 July 2017
#
# copyright (c) Shore2ship BV
#
##############################################################################

id=$(uci get shore2ship.general.serial)
limit=$(uci get shore2ship.3g.datalimit)
dayofmonth=$(date '+%d')
totalusage=$(uci get shore2ship.status.3gusage )
totalusage=$(( $totalusage/1000 ))
date=$(date '+%m/%d/%Y %H:%M')
echo "{ timestamp: \"$date\" , id: $id , metric:85, ttl:3600, value: $totalusage}"

uci set shore2ship.status.usage=$totalusage

if [ $totalusage -gt $limit ]
then
	>&2 echo "Limit exceeded --> applying firewall rules" 
	uci show shore2ship.3g.rules|awk -F"=" '{print $2}' |tr ' ' '\n'|
	while read rule
	do
		>&2 echo "rule=$rule"
		/root/scripts/firewallrule.sh $rule disable
	done
fi

