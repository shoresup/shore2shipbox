module("luci.controller.gateway.gatewaycontrol",package.seeall)

function index()
	page = entry({"admin", "gateway"},"", _("460 Gateway"),20)
	page.sysauth = "gateway"
	page.dependent = false	
	page = entry({"gateway","firewall"}, call("action_firewall"),"",10)
	page.sysauth = "gateway"
	page.dependent = false
	page = entry({"gateway","readfirewall"}, call("read_firewall"),"",10)
    page.sysauth = "gateway"
	page.dependent = false	
	page = entry({"gateway","readprotocols"}, call("read_protocols"),"",10)
    page.sysauth = "gateway"
	page.dependent = false	
	page = entry({"admin","gateway","control"}, template("gateway/s2s"),"Control",10)
	page.sysauth = "gateway"
	page.dependent = false
	page = entry({"gateway","backup"}, call("action_backup"),"Backup",10)
	page.sysauth = "root"
	page.dependent = false
	page = entry({"gateway","rollback"}, call("action_rollback"),"Rollback",10)
	page.sysauth = "root"
	page.dependent = false
	page = entry({"gateway","latestbackup"}, call("action_backupgetlatest"),"Backup",10)
	page.sysauth = "root"
	page.dependent = false
	page = entry({"admin","gateway","antivirus"},call("action_antiviruslog"),_("Anti Virus"),10)
	page.sysauth = "gateway"
	page.dependent = false
	page = entry({"admin","gateway","connlog"},call("action_connectionlog"),_("Connection Log"),10)
	page.sysauth = "gateway"
	page.dependent = false
	page = entry({"admin","gateway","connections"},template("admin_status/connections"),_("Live Connections"),10)
	page.sysauth = "gateway"
	page.dependent = false
end

function action_backup()
	luci.sys.exec("/root/scripts/backupconfig.sh")
	luci.sys.exec("/root/scripts/phaser.sh")
end

function action_rollback()
	luci.sys.exec("/root/scripts/disconnectsound.sh")
	luci.sys.exec("/root/scripts/rollbackconfig.sh")
	//local http = require "luci.http"
	//http.redirect(luci.dispatcher.build_url('admin/system/flashops'))
	--luci.template.render("admin_system/applyreboot")
end





function action_backupgetlatest()
	local backuplist = luci.sys.exec("ls -lt /root/backup|head -1|awk -F\" \" \'{print $9}\'")
	luci.http.write(backuplist)
end

function action_antiviruslog()
	local avlog = luci.sys.exec("clamscan --version");
	luci.template.render("gateway/antivirus", {syslog=avlog})
end

function action_connectionlog()
	local connlog = luci.sys.exec("cat /var/log/ulogd_syslogemu.log");
	luci.template.render("gateway/connlog", {syslog=connlog})
end

function action_firewall()

	local ruletochange = luci.http.formvalue("rule")
	local action = luci.http.formvalue("action")
	
	local utl = require "luci.util"
	
	if ruletochange=='openvpn' and action=='1' then	
		luci.sys.exec("/etc/init.d/openvpn start");
	else
		luci.sys.exec("/etc/init.d/openvpn stop");
	end
	
	
	-- 9-march-2020 exchanged by shellscript
	--luci.model.uci:foreach("firewall", "rule",
	--function(s)
    -- for key, value in pairs(s) do
	--	 if value==ruletochange or value=='dns' then
	--		luci.model.uci:set("firewall",s[".name"],"enabled",action)
	--		luci.model.uci:commit("firewall")
	--	 end	
    --end	
	--end)

	--luci.model.uci:commit("firewall")
	if action=='1' then
		luci.sys.exec("/root/scripts/firewallrule.sh "..ruletochange.." enable")
		luci.sys.exec("/root/scripts/phaser.sh")
	else
		luci.sys.exec("/root/scripts/disconnectsound.sh")
	end	
end


function read_firewall()
	local rule = luci.http.formvalue("rule")
	local ruleinfo = {}

	luci.model.uci:foreach("firewall", "rule",
	function(s)
     for key, value in pairs(s) do
		 if value==rule then 
			ruleinfo["rulestate"]=luci.model.uci:get("firewall",s[".name"],"enabled")
		 end	
     end	
	end)
	ruleinfo["usage"] = luci.model.uci:get("gateway",rule,"usage")
	ruleinfo["icon"] = luci.model.uci:get("gateway",rule,"icon")
	ruleinfo["themecolor"] = luci.model.uci:get("gateway",rule,"themecolor")
	ruleinfo["inactivetimer"] = luci.model.uci:get("gateway",rule,"inactivetimer")
	ruleinfo["activetimer"] = luci.model.uci:get("gateway",rule,"totaltimer")
	ruleinfo["title"] = luci.model.uci:get("gateway",rule,"title")	
	ruleinfo["port"] = luci.model.uci:get("gateway",rule,"port")
	luci.http.write_json(ruleinfo)

end

function read_protocols()
	local protocollist= {}
	luci.model.uci:foreach("gateway", "gatewayrule", function (s)
														table.insert(protocollist,s[".name"])	
													 end  )
													
	luci.http.write_json(protocollist)
end
