#
#
# retrieves pilots from NGA website
#
#
pilotno="110 111 112 113 114 115 116"
for nr in $pilotno
do
	filename="/data/services/lightlists/Pub$nr""bk.pdf"
	if [ ! -f $filename ] 
	then
		url="http://msi.nga.mil/MSISiteContent/StaticFiles/NAV_PUBS/NIMA_LOL/Pub$nr/Pub$nr""bk.pdf"
		wget -P /data/services/lightlists $url
	fi
done
