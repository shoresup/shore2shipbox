#!/bin/bash

timestart=$(date +%s)
pos1=$(/home/pi/position.py)
latlon=($pos1)
lon1=${latlon[0]}
lat1=${latlon[1]}
foc=$(/home/pi/fuelread.py)
foc1=($foc)
focvalue=${foc1[0]}
foctype=${foc1[1]}
if [ $foctype == 'HFO' ]
then
        fochfo=$focvalue
        focmgo=0
else
        focmgo=$focvalue
        fochfo=0
fi
pos2=$(/home/pi/position.py)
timestop=$(date +%s)
timedif=$(($timestop - $timestart - 2))
latlon=($pos2)
lon2=${latlon[0]}
lat2=${latlon[1]}
sogcog=$(/home/pi/speedheading.py $lon1 $lat1 $lon2 $lat2)
sh=($sogcog)
distance=${sh[0]}
course=${sh[1]}
temp=$((3600 / $timedif))
speed=$(echo $distance $temp | awk '{printf "%4.3f\n",$1*$2}')
echo $speed
echo $course
echo $focvalue
date=$(date '+%m/%d/%Y %H:%M')
url=http://live.efficienship.com/datafeed/put
postdata="{ timestamp: \"$date\" , id: 1085 , metric:25, ttl:3600, value: $focmgo}"
echo $postdata
echo $postdata|curl -H "Content-Type: application/json" -X POST -d @- $url
postdata="{ timestamp: \"$date\" , id: 1085 , metric:63, ttl:3600, value: $fochfo}"
echo $postdata
echo $postdata|curl -H "Content-Type: application/json" -X POST -d @- $url
postdata="{ timestamp: \"$date\" , id: 1085 , metric:7, ttl:3600, value: $speed}"
echo $postdata
echo $postdata|curl -H "Content-Type: application/json" -X POST -d @- $url
postdata="{ timestamp: \"$date\" , id: 1085 , metric:18, ttl:3600, value: $course}"
echo $postdata
echo $postdata|curl -H "Content-Type: application/json" -X POST -d @- $url
postdata="{ timestamp: \"$date\" , id: 1085 , metric:47, ttl:3600, value: $lat2}"
echo $postdata
echo $postdata|curl -H "Content-Type: application/json" -X POST -d @- $url
postdata="{ timestamp: \"$date\" , id: 1085 , metric:48, ttl:3600, value: $lon2}"
echo $postdata
echo $postdata|curl -H "Content-Type: application/json" -X POST -d @- $url
