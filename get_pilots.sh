#
#
# retrieves pilots from NGA website
#
#
pilotno="123 124 125 126 127 131 132 141 142 143 145 146 147 148 153 154 155 157 158 159 161 162 163 164 171 172 173 174 175 181 182 183 191 192 193 194 195"
for nr in $pilotno
do
	filename="/data/services/pilots/Pub$nr""bk.pdf"
	if [ ! -f $filename ] 
	then
		url="http://msi.nga.mil/MSISiteContent/StaticFiles/NAV_PUBS/SD/Pub$nr/Pub$nr""bk.pdf"
		wget -P /data/services/pilots $url
	fi
done
