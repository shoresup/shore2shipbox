#!/bin/sh
datafeedID=$(uci get shore2ship.general.serial)
url="https://dashboard.shore2ship.com/api/vessels/getlimit/$datafeedID"
echo $url
newlimit=$(curl -k $url|awk -F"\"" '{ print $4}' )
echo $newlimit
if [ $newlimit -gt 0 ];
then
	uci set shore2ship.3g.datalimit=$newlimit
	uci commit
fi
