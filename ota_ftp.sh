#
# ota_ftp.sh
# shell script for over the air (OTA) update of efficienship communicator device
# v0.2  jan 2013
# (c) 2013 Efficienship BV
# by W.Amels
#
#
source /root/scripts/config.sh
#
datum=$(date)
echo $SERIAL:$datum:starting ftp ota update...>>$LOGFILE
#ifup 3G
#sleep 30
curl -u master:ftp01 -P - ftp://$OTASERVER/$SERIAL/manifest.txt |
while read line
do
	filename=$(echo "$line"| awk -F':' '{ print $1 }')
	todir=$(echo "$line"| awk -F':' '{ print $2 }')
	permission=$(echo "$line"| awk -F':' '{ print $3 }')
	echo "$filename - $todir - $permission"
	length=${#filename}
	echo length:$length
	if [ $length -gt 3 ]
	then 
		echo $todir/$filename
		echo $SERIAL:$datum:ftp retrieving $filename... >>$LOGFILE
		curl -u master:ftp01 -P - ftp://$OTASERVER/$SERIAL/$filename > $todir/$filename
		chmod $permission $todir/$filename	
	fi
done	
echo $SERIAL:$datum:ftp update completed... >>$LOGFILE
#ifdown 3G
#reboot
