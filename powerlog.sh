DAYHOUR=$(date '+%Y%m%d%H')
MINUTE=$(date '+%M')
TIMESTAMP=$(date '+%d/%m/%Y %H:%M:%S')
DAY=$(date '+%Y%m%d')
TIME=$(date '+%H:%M')
echo day=$DAYHOUR
echo min=$MINUTE
SERIAL=9617ET
FILEPOWER=/root/temp/9617ET_"$DAYHOUR"_powerlog.CSV
FILEPV1=/root/temp/9617ETPV1_"$DAYHOUR"_powerlog.CSV
FILEPV2=/root/temp/9617ETPV2_"$DAYHOUR"_powerlog.CSV

#YOULESS=$(curl http://192.168.2.14/a|grep Watt|awk '{print $1}')

ECTUAL=$(/root/scripts/ectual_read.py|grep Watt)
echo ectual=$ECTUAL
YOULESS=$(echo $ECTUAL|awk '{print $2}')
PV1=$(curl http://192.168.2.177|grep PV1|awk '{print $2}')
PV2=$(curl http://192.168.2.177|grep PV2|awk '{print $2}')
TOTALPV=$(($PV1+$PV2))
echo power = $YOULESS
echo PV1 = $PV1
echo PV2 = $PV2

if [ -f $FILEPOWER ]
then
	echo $TIMESTAMP,$YOULESS>>$FILEPOWER
else
	echo "Timestamp,Power (Watt)" >$FILEPOWER
fi


if [ -f $FILEPV1 ]
then
	echo $TIMESTAMP,$PV1>>$FILEPV1
else
	echo "Timestamp,Power (Watt)" >$FILEPV1
fi

if [ -f $FILEPV2 ]
then
	echo $TIMESTAMP,$PV2>>$FILEPV2
else
	echo "Timestamp,Power (Watt)" >$FILEPV2
fi

mod=$(($MINUTE%10))
if [ $mod -eq 0 ]
then
	curl -d "d=$DAY" -d "t=$TIME" -d "v2=$TOTALPV" -H "X-Pvoutput-Apikey: dc17745a332ec2321d2a8a2bc041e3522ab9ad45" -H "X-Pvoutput-SystemId: 29065" http://pvoutput.org/service/r2/addstatus.jsp
fi		
