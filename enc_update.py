#!/usr/bin/python3

import urllib,json,urllib.request,zipfile,os,shutil


#load data about most recent download
if (os.path.exists('lastupdate.txt')):
        with open('lastupdate.txt') as infile:
                localdata=json.load(infile)
                lastpath=localdata[0]["Path"]
else:
        lastpath=''


#retrieve actual update status from somewhere
url="http://api.shore2ship.com/api/update?vesselid=201"
response = urllib.request.urlopen(url)
data=json.loads(response.read().decode('utf-8'))
print(data[0]["Path"])
newpath=data[0]["Path"]
newname=data[0]["Path"].rsplit('/',1)[1]
print(newname)

#f newer data is available download update and unzip it
if (newpath!=lastpath):
        folder = '/data/services/enc/'
        for the_file in os.listdir(folder):
                file_path = os.path.join(folder, the_file)
                if os.path.isfile(file_path):
                        os.unlink(file_path)
                elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
        urllib.request.urlretrieve(data[0]["Path"],"/data/services/enc/"+newname)
        zip_ref=zipfile.ZipFile("/data/services/enc/"+newname,'r')
        zip_ref.extractall("/data/services/enc/")
        zip_ref.close()
        with open('lastupdate.txt','w') as outfile:
                json.dump(data,outfile)
