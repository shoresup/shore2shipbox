#!/usr/bin/python

import socket,os,serial,datetime,time,select,re
# Set the socket parameters
addr = ('', 10110)  # host, port
sendaddr = ('192.168.200.255', 10110)  # host, port

#empty incoming buffer
def empty_socket(sock):
        input = [sock]
        while 1:
                inputready, o, e = select.select(input,[],[], 0.0)
                if len(inputready)==0: break
                for s in inputready: s.recv(1)

#calculate nmea checksum
def checksum(sentence):

    """ Remove any newlines """
    if re.search("\n$", sentence):
        sentence = sentence[:-1]

    nmeadata = sentence

    calc_cksum = 0
    for s in nmeadata:
        calc_cksum ^= ord(s)

    """ Return the nmeadata, the checksum from
        sentence, and the calculated checksum
    """
    return hex(calc_cksum)



# Create socket and bind to address
UDPSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
UDPSock.bind(addr)
UDPSendSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
UDPSendSock.setsockopt(socket.SOL_SOCKET,socket.SO_BROADCAST,1)
i=0
# Receive messages
while i<6:
    empty_socket(UDPSock)
    data, addr = UDPSock.recvfrom(1024)
    if data == 'stop':
           print 'Client wants me to stop.'
           break
    else:
                #print "From addr: '%s', msg: '%s'" % (addr[0], data)
                sentence=data
                parts=sentence.split(',')
                if (parts[0] == '$GPGLL'):
                        newsentence = 'GPGGA,'+ parts[5]+','+ parts[1]+','+parts[2]+','+parts[3]+','+parts[4]+',1,12,0,20,M,34,M,,'
                        cs = checksum(newsentence)
                        newsentence = '$' + newsentence + '*' + cs[2:]
                        print newsentence
                        UDPSendSock.sendto(newsentence,sendaddr)
                        i=i+1
                        time.sleep(10)
