#
# syncer_ftp.sh
# shell script for retrieval of logfiles from Eefting Energy fuel flow meter
# v0.2 jan  2013
# (c) 2013 Efficienship BV
# by W.Amels
#
#
source /root/scripts/config.sh
#
datum=$(date)
echo $SERIAL:$datum:starting ftp retrieval...>>$LOGFILE
month=$(date '+%Y%m')
day=$(date '+%Y%m%d') 
echo today=$day
curl -u master:ftp01 ftp://$FUELMON/REPORT/SAMPLE/$((month))/$((day))/|
while read line
do
	filename=$(echo "$line"| awk '{print $NF}')
	length=${#filename}
	echo length:$length
	if [ $length -gt 3 ]
	then 
		echo $SERIAL:$datum:ftp retrieving $filename... >>$LOGFILE
		fileout=/root/temp/F0006_$((day))_$filename
		curl -u master:ftp01 ftp://10.15.125.81/REPORT/SAMPLE/$((month))/$((day))/$filename > $fileout
		curl -u admin:PHPW -T $fileout ftp://10.15.125.1/Download/Efficienship/
		gzip -f -9 $fileout 
	fi
done	
echo $SERIAL:$datum:ftp retrieval completed... >>$LOGFILE
