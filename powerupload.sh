#**********************
#
# syncer_http.sh    
# (c) 2013 Efficienship BV
# v 1.0 feb-2013
# by W.Amels
#
# ash shell script to upload logfiles to www.efficienship.com
#
#
#
# read settings:
#
#
SERIAL=9617ET
SERVER=live.efficienship.com
datum=$(date)
echo $SERIAL:$datum":starting upload...." >>$LOGFILE
month=$(date '+%Y%m')
day=$(date '+%Y%m%d') 
monthsingle=$(date '+%m')
monthsingle=$(echo $monthsingle | sed 's/^0*//')
echo ms=$monthsingle
digest='efficienship'$SERIAL$monthsingle
echo digest=$digest
tokenraw=$(echo -n $digest|sha1sum)
tokenstrip=${tokenraw:0:40}
echo month=$month
token=$(echo $tokenstrip|awk '{print toupper($0)}')
echo token=$token


echo today=$day
cd /root/temp
gzip *.CSV
ls -l *.gz |
while read line
do
	echo line:$line
	filename=$(echo "$line"| awk '{print $NF}')
	result=$(curl --compressed  -F filename=@$filename http://$SERVER/Uploaddataform.aspx?SERIAL=$SERIAL&TOKEN=$TOKEN|awk '/FL1/ { print $0 }')
	echo $datum:result:$result
	if [ "$result" == "<span id=\"FL1\">Success..</span>" ]
	then
		rm $filename
	else
		echo fail...
	fi
done
gzip -d *.gz
