#
# Copyright (c) Shore2ship
#
# downloads weekly NTMS automatically from Admiralty
#
# version 0.1: August 2017
#
# usage: add crontab entry to run every week
#
weeknrs="01 02 03 04 05"
week=1
while [ $week -lt 53 ]
do
#	weeknr=$(date '+%V')
	weeknr=$(printf "%02d" $week)
	yy=$(date '+%y')
	YY=$(date '+%Y')
	filename=/data/services/ntms/$YY/$weeknr'wknm'$yy'_Week'$weeknr'_'$YY'.pdf'
	dirnam=/data/services/ntms/$YY
	chmod o+r $dirnam
	chmod o+x $dirnam
	if [ ! -f $filename ]
	then	
		echo $weeknr,$yy,$YY
		url='https://www.admiralty.co.uk/WeeklyNMs/Year%20-%20'$YY'/'$weeknr'wknm'$yy'_Week'$weeknr'_'$YY'.pdf'
		echo $url
		found=$(curl -I $url|grep 404 >/dev/null && echo no|| echo yes)
		if [ $found == yes ]
		then	
			curl $url --create-dirs -o $filename
		fi
	fi
	week=$(($week+1))
done
