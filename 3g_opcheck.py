#!/usr/bin/python
##############################################################################
#
# 3g_opcheck.py
#
# v 1.0 June 2016
# v 1.1 May 2018 : select numeric outputmode on modem
#
# copyright (c) Shore2ship BV
#
# checks for operator code and determines active country
#
#

import serial, time,  mmap
ser=serial.Serial('/dev/ttyUSB2',9600,timeout=1)
ser.write('AT+COPS=3,2\x0D')
ser.readline()
ser.readline()
ser.flushInput()
x=0;
f=open('/root/temp/3glog','a')


def find_country(opcode):
#	print opcode
	opcode=opcode[1:6]
	countrycode = opcode[0:3]
	out='nf'
	with open('/root/scripts/truphone.txt','r') as inf:
		for line in inf:
			if countrycode in line:
				out = line.partition('\x09')
				out = out[2]
	return(out);



f.write(time.strftime("%a, %d %b %Y %H:%M:%S",time.gmtime())+ '\n')
ser.write('AT+COPS?\x0D')
time.sleep(1)
out1= ser.readline()
out2= ser.readline()
out3= ser.readline()
out4=ser.readline()
out5=ser.readline()
#print '|'+out1+'|'+out2+'|'+out3+'|'+out4+'|'+out5+'|'
parts = out2.partition(':')
if (parts[0] == '+COPS' and len(parts[2])>5 ):
	values = parts[2].split(',')
	print 'country=' + find_country(values[2])
	f.write( 'Operator=' + values[2] + '\n')
else:
	print 'not registered'
