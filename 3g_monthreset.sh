# 3g_monthreset.sh
#
# run once a month to reset counters
#
sleep 45
uci set shore2ship.status.lastusage=0
uci set shore2ship.status.3gusage=0
uci commit
