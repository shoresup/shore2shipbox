#!/bin/sh
# 
# scanforvirus.sh
#
# versione 0.1: june 2020
# coyright (c) Shore2ship
#
scanresult=$(clamdscan --move=/data/quarantine /data/uncontrolled| grep FOUND >/dev/null && echo FOUND || echo OK)
if [ $scanresult == OK ];
then
	rsync -r /data/uncontrolled/ /data/controlled
else
	/root/scripts/alarm.sh
fi