#/bin/sh

WAN=`uci -P/var/state get network.wan.ifname`
#WANMAC=`ifconfig \$WAN | awk '/HWaddr/ { print $5 }' | sed 's/:/-/g'`
WLAN="wlan0"
UAMHOMEPAGE="https:\/\/customer.hotspotsystem.com\/customer\/index.php?nasid=Shore2ship"
#
#UAMHOMEPAGE=""
WHITELABEL="https:\/\/customer.hotspotsystem.com"
uci set wireless.@wifi-device[0].disabled=0; uci commit wireless; wifi
#WLAN=`ifconfig | grep wl | sort | head -1 | cut -d " " -f1`
WLANMAC=`ifconfig \$WLAN | awk '/HWaddr/ { print $5 }' | sed 's/:/-/g'`

#opkg update
#opkg install coova-chilli 
#opkg install kmod-tun 
wget -O /root/temp/chilli http://www.hotspotsystem.com/firmware/openwrt/chilli 
#chmod a+x /etc/init.d/chilli
wget -O /root/temp/defaults.tmp http://hotspotsystem.com/firmware/openwrt/defaults 
cat /root/temp/defaults.tmp | sed "s/HS_NASID=\"xxxxx\"/HS_NASID=\"Shore2ship\"/g" | sed "s/HS_WANIF=wan/HS_WANIF=$WAN/g" | sed "s/HS_LANIF=lan/HS_LANIF=$WLAN/g" | sed "s/HS_UAMHOMEPAGE=\"\"/HS_UAMHOMEPAGE=\"$UAMHOMEPAGE\"/g" | sed "s/https:\/\/customer.hotspotsystem.com/$WHITELABEL/g" > /etc/chilli/defaults
sed -i -e "/HS_UAMDOMAINS/d" /etc/chilli/defaults
#echo HS_UAMDOMAINS=\"paypal.com paypalobjects.com worldpay.com rbsworldpay.com adyen.com hotspotsystem.com geotrust.com triodos.nl asnbank.nl knab.nl regiobank.nl snsbank.nl \" >> /etc/chilli/defaults




chillistart=0
wanif=0

chillihotplug="/etc/hotplug.d/iface/30-chilli"
chillirc="/etc/init.d/chilli"
netconfig="/etc/config/network"

echo -n "$chillihotplug: "
if [ -f "$chillihotplug" ]; then
  echo "found"
  chillistart=`grep $chillirc $chillihotplug | wc -l`
  echo -n "Manual chilli service start command in $chillihotplug: "
  if [ "$chillistart" -gt "0" ]; then
    echo "found"
  else
    echo "not found"
  fi
else
  echo "not found"
fi

echo -n "$chillirc: "
if [ -f "$chillirc" ]; then
  echo "found"
else
  echo "not found"
fi

echo -n "$netconfig: "
if [ -f "$netconfig" ]; then
  echo "found"
  echo -n "WAN interface in $netconfig: "
  wanif=`grep "'wan'" $netconfig | wc -l`
  if [ "$wanif" -gt "0" ]; then
    echo "yes"
  else
    echo "no"
  fi
else
  echo "not found"
fi

if [ "$wanif" -eq "0" ]; then
  echo "Enabling chilli: /etc/init.d/chilli enable"
  /etc/init.d/chilli enable
elif [ "$chillistart" -eq "0" ]; then
  echo "Enabling chilli: /etc/init.d/chilli enable"
  /etc/init.d/chilli enable
else                                                                                                      
  echo "Enabling chilli service startup: /etc/init.d/chilli enable"                                       
  echo "Disabling hotplug chilli startup in $chillihotplug"                                               
  rm $chillihotplug                                                                                       
  /etc/init.d/chilli enable                                                                               
fi                                                                 
                                                                   
echo "Rebooting router..."                                         
