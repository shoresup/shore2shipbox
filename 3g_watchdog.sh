###############################################################################
#
# 3g_watchdog.sh
#
# version 1.0 June 2016
#
# copyright (c) shore2ship
#
###############################################################################

connected=$(ping -q -w 10 -c 1 8.8.8.8 >/dev/null && echo ok || echo error)
if [ $connected == ok ]; 
then
	echo "online"
else
	echo "offline"
	ifup 3g
	sleep 20
	connected2=$(ping -q -w 10 -c 1 8.8.8.8 >/dev/null && echo ok || echo error)
	if [ $connected2 == ok ]; 
	then
		echo "online"
	else	
		vnstat --update
		ifdown 3g
		sleep 10
		/root/scripts/3g_rereg.py
		sleep 10
		ifup 3g
	fi
	sleep 60
	connected3=$(ping -q -w 10 -c 1 8.8.8.8 >/dev/null && echo ok || echo error)
	if [ $connected3 == ok ]; 
	then
		echo "online"
	else
		vnstat --update
		ifdown 3g
		sleep 10
		signal=$(/root/scripts/3g_signal.py)
		echo "signal" $signal
		if [ $signal != 99 ];
		then
			echo "should reboot maybe"
			reboot
		fi
	fi
fi
