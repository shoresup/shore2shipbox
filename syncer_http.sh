#**********************
#
# syncer_http.sh    
# (c) 2013 Efficienship BV
# v 1.0 feb-2013
# by W.Amels
#
# ash shell script to upload logfiles to www.efficienship.com
#
#
#
# read settings:
source /root/scripts/config.sh
#
#
datum=$(date)
echo $SERIAL:$datum":starting upload...." >>$LOGFILE
month=$(date '+%Y%m')
day=$(date '+%Y%m%d') 
monthsingle=$(date '+%m')
monthsingle=$(echo $monthsingle | sed 's/^0*//')
echo ms=$monthsingle
digest='efficienship'$SERIAL$monthsingle
echo digest=$digest
tokenraw=$(echo -n $digest|sha1sum)
tokenstrip=${tokenraw:0:40}
echo month=$month
token=$(echo $tokenstrip|awk '{print toupper($0)}')
echo token=$token


echo today=$day
cd /root/temp
no_of_lines=$(ls -l *.CSV.gz|awk 'END{ print NR}')
echo $SERIAL:$datum:number of files to transfer: $no_of_lines >>$LOGFILE
if [ $no_of_lines == 0 ] 
then
	echo $SERIAL:$datum:nothing to transfer..>>$LOGFILE
fi
#opcheck=$(/root/scripts/3g_opcheck.py)
#echo "$opcheck" >>$LOGFILE
#echo $opcheck
#if [ "$opcheck" == "operator=nf country=nf" ]
#then
#	echo "operator unknown" >>$LOGFILE
#	exit
#fi
#if [ "$opcheck" == "not registered" ]
#then	
#	echo "not registered" >>$LOGFILE
#	exit
#fi
#ifup $INTERFACE
#secondcount=0
#while [ $secondcount -le $IFTIMEOUT ]
#do
#	sleep 1
#	echo $secondcount
#	up=$(ifconfig 3g-3G 2>&1| grep UP|wc -l)
#	secondcount=$(($secondcount+1))
#	if [ $up == 1 ]
#	then 
#		secondcount=99
#	fi
#done
#if [ $up == 0 ]
#then
#	echo $SERIAL:$datum:Connection failed... >>$LOGFILE 
#	ifdown $INTERFACE
#	exit
#fi
#interface is up, first sync time!
#sleep 10
#ntpclient -c 1 -s -h pool.ntp.org
#ntpd -q -p ptbtime1.ptb.de
ls -l *.CSV.gz |
while read line
do
	echo line:$line
	filename=$(echo "$line"| awk '{print $NF}')
	echo $SERIAL:$datum:filename:$filename >>$LOGFILE
	result=$(curl  -F filename=@$filename http://$SERVER/datafeed.aspx?SERIAL=$SERIAL\&TOKEN=$token|awk '/FL1/ { print $0 }')
	echo $datum:result:$result
	if [ "$result" == "<span id=\"FL1\">Success..</span>" ]
	then
		rm $filename
		echo $SERIAL:$datum:removing file:$filename>>$LOGFILE
	else
		echo $SERIAL:$datum:$filename:upload failed..>>$LOGFILE
	fi
done
echo $SERIAL:$datum:"completed..." >>$LOGFILE
rm $LOGFILE.gz
cp $LOGFILE /root/temp/templog
gzip -9 $LOGFILE
mv /root/temp/templog $LOGFILE
result=$(curl -F filename=@$LOGFILE.gz http://$SERVER/datafeed.aspx?SERIAL=$SERIAL\&TOKEN=$token)
if [ "$result" == "<span id=\"FL1\">Success..</span>" ]
then
	rm $LOGFILE
	echo logfile succesfully uploaded, removing..
else
	echo logfile upload failed..
fi
#ifconfig 3g-3G|awk '/bytes/ { print $0 }' >>$LOGFILE
#ifdown $INTERFACE
